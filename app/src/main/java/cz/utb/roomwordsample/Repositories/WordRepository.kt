package cz.utb.roomwordsample.Repositories

import androidx.annotation.WorkerThread
import cz.utb.roomwordsample.DAOs.WordDao
import cz.utb.roomwordsample.Entities.Word

class WordRepository(private val wordDao: WordDao)
{
    val allWords = wordDao.getWords()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(word: Word)
    {
        wordDao.insert(word)
    }
}