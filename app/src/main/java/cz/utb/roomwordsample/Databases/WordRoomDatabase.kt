package cz.utb.roomwordsample.Databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import cz.utb.roomwordsample.DAOs.WordDao
import cz.utb.roomwordsample.Entities.Word
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = arrayOf(Word::class), version = 1, exportSchema = false)
public abstract class WordRoomDatabase : RoomDatabase()
{
    abstract fun wordDao() : WordDao

    companion object
    {
        private var Instance: WordRoomDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope) : WordRoomDatabase
        {
            return Instance ?: synchronized(this)
            {
                val instance = Room.databaseBuilder(context.applicationContext, WordRoomDatabase::class.java, "word_database")
                    .addCallback(WordDatabaseCallback(scope))
                    .build()
                Instance = instance
                instance
            }
        }
    }

    private class WordDatabaseCallback(private  val scope: CoroutineScope) : RoomDatabase.Callback()
    {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            Instance?.let { database ->
                scope.launch {
                    populateDatabase(database.wordDao())
                }
            }
        }

        suspend fun populateDatabase(wordDao: WordDao) {
            wordDao.deleteAll()
            var word = Word("Ahoj")
            wordDao.insert(word)
            word = Word("Světe!")
            wordDao.insert(word)
        }
    }
}
